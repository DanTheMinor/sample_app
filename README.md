# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*] (http://www.railstutorial.org/)
by [Micheal Hartl](http://www.michealhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://www.railstutorial.org/)
is availible jointly under the MIT License and Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with this app, clone the repo then install the neccesary gems:

```
$ bundle install --without production
```

Next, migrate the database:
```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly
```
$ rails test
```

If the test suite passes, you'll be ready to run the application on a local server:
```
rails server
```

For more information, see the
[*Ruby On Rails Tutorial* book](http://www.railstutorial.org/book).
